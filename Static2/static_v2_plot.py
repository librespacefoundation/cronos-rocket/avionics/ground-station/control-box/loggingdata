# The only reason for the existence of this code is to be able to use matplotlib's interactive plot viewer 
import matplotlib.pyplot as plt 

data = { 'lr': [],'lc1': [],'lc2': [],'lc3': [],'mf': [],'mq': [],'mt': [],'mr': [],'mp': [],'p': [], 'wt': [],'wf': [],'wp': [],'pr': [],'pi': [],'pc': []}
times = {'lr': [],'lc1': [],'lc2': [],'lc3': [],'mf': [],'mq': [],'mt': [],'mr': [],'mp': [],'p': [], 'wt': [],'wf': [],'wp': [],'pr': [],'pi': [],'pc': []}
keys = list(data.keys())

# ====================== Break down raw datastream ==================

with open("static_v2_data.txt", "r") as f:
    for l in f.readlines():
        line = l.replace('\n', '')
        splited = line.split(': ')
        if len(splited) > 2:
            continue

        date = splited[0]

        data_ = splited[1].split(',')[1:]

        for d in data_:
            try:
                type_ = d.split(':')[0]
                value = d.split(':')[1]

                if type_ in data:
                    data[type_].append((date, value))
            except:
                continue


for i in keys: 
    start_time = float(data[i][0][0]) 

    for j in range(len(data[i])):
        # times[i].append(float(data[i][j][0]) - start_time)
        times[i].append((float(data[i][j][0]) - start_time)/1000) # for times in seconds
        data[i][j] = float(data[i][j][1]) # convert data from strings to floats
        
# determine the time when the actual test starts
for i in range(len(data["p"])-1):
    if (data['p'][i+1] != data['p'][i]): 
        test_start_time_idx = i
        test_start_time = times['p'][i]
        break 

# Mark test start time as zero time (list length is kept the same, so we also set xlim to start from 0)
for i in keys : 
    for j in range(len(data[i])):
        times[i][j] = times[i][j] - test_start_time

# convert thrust to Newton 
for i in range(len(data['lc1'])): 
    data['lc1'][i] = 9.81 * data['lc1'][i]

# injector and CC pressure transducers had been swapped during the test, so we swap their measurements here
placeholder_data = data['pc']
placeholder_times = times['pc']
data['pc'] = data['pi']
times['pc'] = times['pi']
data['pi'] = placeholder_data 
times['pi'] = placeholder_times

# =============================== Cumulative plot ==============================
# plt.figure(1)

fig,ax = plt.subplots(figsize = (20,9))
# axins = ax.inset_axes([0.1,0.5,0.4,0.4])
ax.set_title("MK2 Static Test v2 Cumulative plot")
ax.set_xlabel("Time [sec]")
handles = []
ax1 = ax.twinx()
ax2 = ax.twinx()
ax2.spines.right.set_position(("axes", 1.05))

l1, = ax.plot(times['lr'],data['lr'], label = "RTLC")
# axins.plot(times['lr'],data['lr'])
handles.append(l1)

pot_scaling_factor = max(data['wf']) / max(data['lr'])
data['wf'] = [i/pot_scaling_factor for i in data['wf']]
data['wp'] = [i/pot_scaling_factor for i in data['wp']]
data['wt'] = [i/pot_scaling_factor for i in data['wt']]

l2, = ax.plot(times['wf'],data['wf'], label = "Fill Pot", alpha = 0.5)
# axins.plot(times['wf'],data['wf'],alpha = 0.5)
handles.append(l2)

l3, = ax.plot(times['wp'],data['wp'], label = "Purge Pot", alpha = 0.5)
# axins.plot(times['wp'],data['wp'],alpha = 0.5)
handles.append(l3)

l4, = ax.plot(times['wt'],data['wt'], label = "Thrust Pot", alpha = 0.5)
# axins.plot(times['wt'],data['wt'],alpha = 0.5)
handles.append(l4)

l5, = ax1.plot(times['lc1'],data['lc1'],label = "Thrust", c= "purple")
handles.append(l5)

l6, = ax2.plot(times['pi'],data['pi'], label = "$P_{inj}$", c = "black")
handles.append(l6)

l7, = ax2.plot(times['pc'],data['pc'], label = "$P_{cc}$", c="mediumturquoise")
handles.append(l7)

l8, = ax2.plot(times['pr'],data['pr'], label = "$P_{rt}$", c = "red")
handles.append(l8)


ax.set_xlim(0,60)
ax.legend(handles = handles,prop={'size': 14})
ax.grid(linestyle = '--')

ax.set_ylabel("Run Tank Weight [kg]")

ax1.set_ylabel("Thrust [N]")

ax2.set_ylabel("Pressure [bar]")

# plt.rcParams['figure.dpi'] = 800
# plt.savefig("cumulative_plot.png",dpi = 800)


# =============================== Thrust plot ==============================
# plt.figure(2)
fig,ax = plt.subplots()
# axins = ax.inset_axes([0.1,0.5,0.4,0.4])
ax.set_title("MK2 Static Test v2 Time - Thrust")
ax.set_xlabel("Time [sec]")
handles = []

l5, = ax.plot(times['lc1'],data['lc1'],label = "Thrust", c= "purple")
handles.append(l5)


ax.set_xlim(0,60)
ax.legend(handles = handles,prop={'size': 14})
ax.grid(linestyle = '--')


ax.set_ylabel("Thrust [N]")

# plt.rcParams['figure.dpi'] = 800
# plt.savefig("time_thrust.png",dpi = 800)


# =============================== pots plot ==============================
# plt.figure(3)
fig,ax = plt.subplots()
# axins = ax.inset_axes([0.1,0.5,0.4,0.4])
ax.set_title("MK2 Static Test v2 Time - Pots")
ax.set_xlabel("Time [sec]")
handles = []

pot_scaling_factor = max(data['wf']) / max(data['lr'])
data['wf'] = [i/pot_scaling_factor for i in data['wf']]
data['wp'] = [i/pot_scaling_factor for i in data['wp']]
data['wt'] = [i/pot_scaling_factor for i in data['wt']]

l2, = ax.plot(times['wf'],data['wf'], label = "Fill Pot", alpha = 0.5)
# axins.plot(times['wf'],data['wf'],alpha = 0.5)
handles.append(l2)

l3, = ax.plot(times['wp'],data['wp'], label = "Purge Pot", alpha = 0.5)
# axins.plot(times['wp'],data['wp'],alpha = 0.5)
handles.append(l3)

l4, = ax.plot(times['wt'],data['wt'], label = "Thrust Pot", alpha = 0.5)
# axins.plot(times['wt'],data['wt'],alpha = 0.5)
handles.append(l4)

ax.set_xlim(0,60)
ax.legend(handles = handles,prop={'size': 7})
ax.grid(linestyle = '--')


# plt.rcParams['figure.dpi'] = 800
# plt.savefig("time_pots.png",dpi = 800)


# =============================== pressure plot ==============================
# plt.figure(4)
fig,ax = plt.subplots()
# axins = ax.inset_axes([0.1,0.5,0.4,0.4])
ax.set_title("MK2 Static Test v2 Time - Pressure")
ax.set_xlabel("Time [sec]")
handles = []

l6, = ax.plot(times['pi'],data['pi'], label = "$P_{inj}$")
handles.append(l6)

l7, = ax.plot(times['pc'],data['pc'], label = "$P_{cc}$")
handles.append(l7)

l8, = ax.plot(times['pr'],data['pr'], label = "$P_{rt}$")
handles.append(l8)


ax.set_xlim(0,60)
ax.legend(handles = handles,prop={'size': 14})
ax.grid(linestyle = '--')


ax.set_ylabel("Pressure [bar]")

# plt.rcParams['figure.dpi'] = 800
# plt.savefig("time_pressure.png",dpi = 800)



# =============================== RTLC plot ==============================
# plt.figure(5)
fig,ax = plt.subplots()
# axins = ax.inset_axes([0.1,0.5,0.4,0.4])
ax.set_title("MK2 Static Test v2 Time - Pressure")
ax.set_xlabel("Time [sec]")
handles = []

l1, = ax.plot(times['lr'],data['lr'], label = "RTLC", c= "purple")
# axins.plot(times['lr'],data['lr'])
handles.append(l1)

ax.set_xlim(0,60)
ax.legend(handles = handles,prop={'size': 14})
ax.grid(linestyle = '--')


ax.set_ylabel("Run Tank Weight [kg]")

# plt.rcParams['figure.dpi'] = 800
# plt.savefig("time_rtlc.png",dpi = 800)
plt.show()