import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
from datetime import datetime

# ========================================= STATIC V2 DATA =============================================

data = { 'lr': [],'lc1': [],'lc2': [],'lc3': [],'mf': [],'mq': [],'mt': [],'mr': [],'mp': [],'p': [], 'wt': [],'wf': [],'wp': [],'pr': [],'pi': [],'pc': []}
times = {'lr': [],'lc1': [],'lc2': [],'lc3': [],'mf': [],'mq': [],'mt': [],'mr': [],'mp': [],'p': [], 'wt': [],'wf': [],'wp': [],'pr': [],'pi': [],'pc': []}
keys = list(data.keys())


with open("static_v2_data.txt", "r") as f:
    for l in f.readlines():
        line = l.replace('\n', '')
        splited = line.split(': ')
        if len(splited) > 2:
            continue

        date = splited[0]

        data_ = splited[1].split(',')[1:]

        for d in data_:
            try:
                type_ = d.split(':')[0]
                value = d.split(':')[1]

                if type_ in data:
                    data[type_].append((date, value))
            except:
                continue


for i in keys: 
    start_time = float(data[i][0][0]) 

    for j in range(len(data[i])):
        # times[i].append(float(data[i][j][0]) - start_time)
        times[i].append((float(data[i][j][0]) - start_time)/1000) # for times in seconds
        data[i][j] = float(data[i][j][1]) # convert data from strings to floats
        
# determine the time when the actual test starts
for i in range(len(data["p"])-1):
    if (data['p'][i+1] != data['p'][i]): 
        test_start_time_idx = i
        test_start_time = times['p'][i]
        break 

# Mark test start time as zero time (list length is kept the same, so we also set xlim to start from 0)
for i in keys : 
    for j in range(len(data[i])):
        times[i][j] = times[i][j] - test_start_time


# ========================================= COLD FLOW 1 DATA =====================================
# Load data 
cf1_pressures = pd.read_csv("../Results-ColdFlow/pressure.txt",delimiter =',', header = None, skiprows = [0])
cf1_pressures = np.array(cf1_pressures)
cf1_lc = pd.read_csv("../Results-ColdFlow/weight.txt",delimiter =',', header = None, skiprows = [0])
cf1_lc = np.array(cf1_lc)

# Extract RT pressure and times lists 
cf1_rt_press = list(cf1_pressures[:,1])
cf1_inj_press = list(cf1_pressures[:,2])
cf1_press_times = list(cf1_pressures[:,0])
cf1_rt_lc = list(cf1_lc[:,1])
cf1_lc_times = list(cf1_lc[:,0])

# Cut data to start time -- the length of the list is preserved, so we also set a 0 lower xlim on the plot
cf1_valvecheck_start = "2022-03-19 13:50:34.226577" # hard-coded because there was no easy way to get it 
start_time = datetime.strptime(cf1_valvecheck_start,'%Y-%m-%d %H:%M:%S.%f')
# start_time = float(start_time.timestamp()*1000) # for milliseconds 
start_time = float(start_time.timestamp()) # for seconds


for i in range(cf1_pressures.shape[0]): 
    current_time = datetime.strptime(cf1_press_times[i],'%Y-%m-%d %H:%M:%S.%f')
    current_time = float(current_time.timestamp())
    cf1_press_times[i] = current_time - start_time

    current_time = datetime.strptime(cf1_lc_times[i],'%Y-%m-%d %H:%M:%S.%f')
    current_time = float(current_time.timestamp())
    cf1_lc_times[i] = current_time - start_time


# ========================================= COLD FLOW 2 DATA =====================================
# Load data 
cf2_pressures = pd.read_csv("../Results Cold Flow 2/pressure.txt",delimiter =',', header = None, skiprows = [0])
cf2_pressures = np.array(cf2_pressures)
cf2_lc = pd.read_csv("../Results Cold Flow 2/weight.txt",delimiter =',', header = None, skiprows = [0])
cf2_lc = np.array(cf2_lc)

# Extract RT pressure and times lists 
cf2_rt_press = list(cf2_pressures[:,1])
cf2_inj_press = list(cf2_pressures[:,2])
cf2_press_times = list(cf2_pressures[:,0])
cf2_rt_lc = list(cf2_lc[:,1])
cf2_lc_times = list(cf2_lc[:,0])

# Cut data to start time -- the length of the list is preserved, so we also set a 0 lower xlim on the plot
cf2_valvecheck_start = "2022-06-14 21:37:21.831375" # hard-coded because there was no easy way to get it 
start_time = datetime.strptime(cf2_valvecheck_start,'%Y-%m-%d %H:%M:%S.%f')
# start_time = float(start_time.timestamp()*1000) # for milliseconds 
start_time = float(start_time.timestamp()) # for seconds


for i in range(cf2_pressures.shape[0]): 
    current_time = datetime.strptime(cf2_press_times[i],'%Y-%m-%d %H:%M:%S.%f')
    current_time = float(current_time.timestamp())
    cf2_press_times[i] = current_time - start_time

    current_time = datetime.strptime(cf2_lc_times[i],'%Y-%m-%d %H:%M:%S.%f')
    current_time = float(current_time.timestamp())
    cf2_lc_times[i] = current_time - start_time


# ========================== Cumulative plot =============================
fig,ax = plt.subplots(figsize = (20,9))
ax1 = ax.twinx()
ax.set_title("Run Tank behavior on CF1-CF2-Static2")
ax.set_xlabel("Time [sec]")
ax1.set_ylabel("Run tank mass [kg]")
ax.set_ylabel("Run tank pressure [bar]")

handles = [] 

l1, = ax.plot(cf1_press_times,cf1_rt_press,label = "CF1 - $P_{rt}$")
handles.append(l1)

l2, = ax.plot(cf2_press_times,cf2_rt_press,label = "CF2 - $P_{rt}$")
handles.append(l2)

l3, = ax.plot(times['pr'],data['pr'],label = "Static 2 - $P_{rt}$")
handles.append(l3)

l4, = ax1.plot(cf1_lc_times,cf1_rt_lc,label = "CF1 - RT LC", c="black")
handles.append(l4)

l5, = ax1.plot(cf2_lc_times,cf2_rt_lc,label = "CF2 - RT LC", c = "mediumturquoise")
handles.append(l5)

l6, = ax1.plot(times['lr'],data['lr'],label = "Static 2 - RT LC",c="red")
handles.append(l6)

plt.legend(handles = handles)
plt.xlim([0,250])

# ========================== Run Tank Pressure plot =============================
fig,ax = plt.subplots()
ax.set_title("Run Tank pressure on CF1-CF2-Static2")
ax.set_xlabel("Time [sec]")
ax.set_ylabel("Run tank pressure [bar]")

handles = [] 

l1, = ax.plot(cf1_press_times,cf1_rt_press,label = "CF1 - $P_{rt}$")
handles.append(l1)

l2, = ax.plot(cf2_press_times,cf2_rt_press,label = "CF2 - $P_{rt}$")
handles.append(l2)

l3, = ax.plot(times['pr'],data['pr'],label = "Static 2 - $P_{rt}$")
handles.append(l3)


plt.legend(handles = handles)
plt.xlim([0,250])


# ========================== Injector Pressure plot =============================
fig,ax = plt.subplots()
ax.set_title("Injector pressure on CF1-CF2-Static2")
ax.set_xlabel("Time [sec]")
ax.set_ylabel("Injector pressure [bar]")

handles = [] 

l1, = ax.plot(cf1_press_times,cf1_inj_press,label = "CF1 - $P_{inj}$")
handles.append(l1)

l2, = ax.plot(cf2_press_times,cf2_inj_press,label = "CF2 - $P_{inj}$")
handles.append(l2)

l3, = ax.plot(times['pi'],data['pi'],label = "Static 2 - $P_{inj}$")
handles.append(l3)


plt.legend(handles = handles)
plt.xlim([0,250])

# ========================== Run tank weight plot =============================
fig,ax = plt.subplots()
ax.set_title("Run Tank weight on CF1-CF2-Static2")
ax.set_xlabel("Time [sec]")
ax.set_ylabel("Run tank mass [kg]")

handles = [] 

l1, = ax.plot(cf1_lc_times,cf1_rt_lc,label = "CF1 - RT LC", c="black")
handles.append(l1)

l2, = ax.plot(cf2_lc_times,cf2_rt_lc,label = "CF2 - RT LC", c = "mediumturquoise")
handles.append(l2)

l3, = ax.plot(times['lr'],data['lr'],label = "Static 2 - RT LC",c="red")
handles.append(l3)

plt.legend(handles = handles)
plt.xlim([-50,250])

plt.show()