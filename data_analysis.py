
data = { 
    'lr': [],
    'lc1': [],
    'lc2': [],
    'lc3': [],
    'mf': [],
    'mq': [],
    'mt': [],
    'mr': [],
    'mp': [],
    'p': [], 
    'wt': [],
    'wf': [],
    'wp': [],
    'pr': [],
    'pi': [],
    'pc': []
}


with open("file.txt", "r") as f:
    for l in f.readlines():
        line = l.replace('\n', '')
        splited = line.split('[] ')
        if len(splited) > 2:
            continue

        date = splited[0]

        data_ = splited[1].split(',')[1:]

        for d in data_:
            try:
                type_ = d.split(':')[0]
                value = d.split(':')[1]

                if type_ in data:
                    data[type_].append((date, value))
            except:
                continue

from pprint import pprint
pprint(data["p"])

