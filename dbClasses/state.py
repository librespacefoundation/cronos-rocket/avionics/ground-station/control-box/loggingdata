import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

from .base import Base 

class State(Base):
    __tablename__ = "state"

    id = db.Column(db.Integer, primary_key=True, unique=True)
    timestamp = db.Column(db.DateTime)
    
    thrustValveState = db.Column(db.String) 
    fillValveState = db.Column(db.String) 
    qrValveState = db.Column(db.String) 
    purgeValveState = db.Column(db.String) 
    reliefValveState = db.Column(db.String)  
    
    progress = db.Column(db.String) 

    