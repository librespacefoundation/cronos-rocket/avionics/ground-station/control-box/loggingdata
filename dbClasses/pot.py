import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

from .base import Base 

class Pot(Base):
    __tablename__ = "pot"

    id = db.Column(db.Integer, primary_key=True, unique=True)
    timestamp = db.Column(db.DateTime)
    
    thrustPot = db.Column(db.Float) 
    fillPot = db.Column(db.Float) 
    purgePot = db.Column(db.Float)   
    